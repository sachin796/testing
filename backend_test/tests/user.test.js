const request = require("supertest");
const app = require("../src/app");
const User = require("../src/models/user");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const userOne = {
  name: "sachin",
  email: "sachin@example.com",
  password: "Pass1234@",
};

test("Should signup a new user", async () => {
  await User.deleteMany();
  await request(app)
    .post("/users")
    .send({
      name: "sachin",
      email: "sachin@example.com",
      password: "Pass1234@",
    })
    .expect(201);
});

test("Should signup a new user max test", async () => {
  await request(app)
    .post("/users")
    .send({
      name: "max",
      email: "max@example.com",
      password: "Pass1234@",
    })
    .expect(201);
});

test("Should not allow max to sign in again", async () => {
  await request(app)
    .post("/users")
    .send({
      name: "max",
      email: "max@example.com",
      password: "Pass1234@",
    })
    .expect(400);
});

// test("Should login existing user", async () => {
//   await request(app)
//     .post("/users/login")
//     .send({
//       _id: userDetails["userID"],
//       email: "sachin@example.com",
//       password: "Pass1234@",
//       token: userDetails.tokens[0].token,
//     })
//     .expect(200);
// });

// test("Should get profile for users", async () => {
//   await request(app)
//     .get("/users/me")
//     .set("Authorization", `Bearer ${userDetails.tokens[0].token}`)
//     .send()
//     .expect(200);
// });
